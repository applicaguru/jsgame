import * as THREE from "three"
import {OrbitControls} from "three/examples/jsm/controls/OrbitControls"
import {FBXLoader} from "three/examples/jsm/loaders/FBXLoader"

const velocity = {
    x: 0,
    y: 0
}
const position = {
    x: 0, 
    y: 0
}

let scene, renderer, camera;
let box, model;
let controls;

export default function main() {
    scene = new THREE.Scene();
    renderer = new THREE.WebGLRenderer({antialias: true});
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.setPixelRatio(window.devicePixelRatio);
    
    const container = document.getElementById("entry-point");
    container.appendChild(renderer.domElement);

    camera = new THREE.PerspectiveCamera(60, window.innerWidth / window.innerHeight, 0.1, 500);
    camera.position.set(0, 2, -5);
    camera.lookAt(0, 0, 0);
    scene.add(camera);

    const sun = new THREE.AmbientLight(0xFFFFFF, 0.5);
    scene.add(sun);

    const light = new THREE.DirectionalLight(0xFFFF97, 2);
    light.position.set(5, 5, -1);
    scene.add(light);

    const geometry = new THREE.BoxGeometry(1, 1, 1);
    const material = new THREE.MeshPhongMaterial({color: 0xFF6600});
    box = new THREE.Mesh(geometry, material);

    scene.background = 0x000000;
    //scene.add(box);

    window.addEventListener("keydown", e => {
        //alert(e.key);
        if (e.key == "ArrowLeft") {
            velocity.x -= 0.01;
        } else if (e.key == "ArrowRight") {
            velocity.x += 0.01;
        } 
    });

    controls = new OrbitControls(camera, renderer.domElement);

    new FBXLoader().load("resources/model.fbx", m => {
        m.scale.set(0.1, 0.1, 0.1);
        scene.add(m);

        model = m;
    });

    loop();
}

function processInput() {

}

function update() {
    //box.rotation.y += 0.01;
    //box.rotation.x += 0.02;
    //box.rotation.z += 0.03;

    position.x += velocity.x;
    position.y += velocity.y;


    box.position.x = position.x;
    box.position.y = position.y;

    if (model) {
    model.rotation.z += 0.01;
        
    }

    controls.update();
}

function render() {
    renderer.render(scene, camera);
}

function loop() {
    processInput();
    update();
    render();

    requestAnimationFrame(loop);
}