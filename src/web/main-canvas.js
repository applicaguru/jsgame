let ctx;
const velocity = {
    x: 1,
    y: 1
}
const position = {
    x: 0, 
    y: 0
}

export default function main() {
    const canvas = document.createElement("canvas");
    canvas.height = window.innerHeight;
    canvas.width = window.innerWidth;

    const container = document.getElementById("entry-point");
    container.appendChild(canvas);

    ctx = canvas.getContext("2d");
    


    loop();
}

function processInput() {

}

function update() {
    position.x += velocity.x;
    position.y += velocity.y;

    if (position.x > window.innerWidth || position.x < 0) {
        velocity.x *= -1;
    }

    if (position.y > window.innerHeight || position.y < 0) {
        velocity.y *= -1;
    }
}

function render() {
    ctx.clearRect(0, 0, window.innerWidth, window.innerHeight);

    ctx.fillStyle = "#FF6600";

    const size = 100;
    ctx.fillRect(position.x, position.y, size, size);
    ctx.stokeStyle = "#000000"
    ctx.strokeRect(position.x, position.y, size, size);
}

function loop() {
    processInput();
    update();
    render();

    requestAnimationFrame(loop);
}